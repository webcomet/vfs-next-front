import React from 'react';
import Nav from '../components/Nav';
import Page from '../layouts/main';
import Footer from '../components/Footer';
import Card from '../components/InfoCard';
import Label from '../components/InfoLabel';
import Map from '../components/Map';

// first background plate on main page
const background = {
	width: '100%',
	height: '100%',
	backgroundSize: 'cover',
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'center'
};

// main header
const headerTitle = {
	color: '#f6f6e9',
	fontSize: '40px',
	lineHeight: '1.2'
};

// main page header subtitles
const subtitle = {
	padding: '30px 0 30px 0',
	color: '#f6f6e9',
	fontSize: '16px'
};

// common button style
const blueBtn = {
	backgroundColor: '#005792',
	borderColor: '#005792',
	borderRadius: '20px',
	color: '#f6f6e9',
	padding: '7px 40px',
	margin: '0 0 200px 0'
};

const darkerBtn = {
	backgroundColor: '#13334c',
	borderColor: '#13334c',
	borderRadius: '20px',
	color: '#f6f6e9',
	padding: '7px 40px'
};

// blue subheader
const blueHeader = {
	color: '#005792',
	padding: '50px 0 30px 0',
	fontSize: '35px',
	fontWeight: '400',
	letterSpacing: '0.1em'
};

const phoneInput = {
	margin: '20px 0 30px 0',
	padding: '5px',
	color: '#f6f6e9',
	backgroundColor: '#3a56a9',
	borderColor: '#f6f6e9',
	borderStyle: 'solid',
	borderRadius: '2px',
	boxSizing: 'border-box',
	width: '65%'
};

const Home = () => (
	<Page>
		{/* Site Header with log and navbar */}
		<div style={{ ...background, backgroundImage: `url(./headerback.jpg)` }}>
			<div className="container">
				<Nav />

				<h1 style={{ ...headerTitle, padding: '200px 0 0 0' }}>
					ЦЕНТР<br /> ПОЛЕТОВ<br /> НА АВИАСИМУЛЯТОРАХ
				</h1>
				<div style={subtitle}>
					Каждый из нас хоть раз в жизни мечтал летать,<br />
					мы поможем Вам воплотить эту мечту в реальность!<br />
					Побалуйте себя незабываемым полётом или<br />
					подарите небо своим близким
				</div>
				{/* todo: native button */}
				<button className="btn btn-primary" style={blueBtn}>
					Заказать полет
				</button>
			</div>
		</div>

		{/* Avaivable programs description (cards part) */}
		<div className="container">
			<div style={blueHeader}>Наши программы полетов</div>
		</div>

		<div className="container d-flex flex-row justify-content-between pb-4">
			<Card
				imageSrc="url(./infocard1.jpg)"
				title="Обучающая программа А-22 Аэропракт"
				text="Идеально подойдет для тех кто хочет познакомиться с авиацией. Мы расскажем о частях самолета, покажем как ориентироваться в воздухе, научим делать взлет-посадку и сделаем перелет между любыми аэропортами мира."
			/>
			<Card
				imageSrc="url(./infocard2.jpg)"
				title="Обучающая программа Миг-21"
				text="Вы научитесь делать запуск реактивного истребителя, выполнять взлет и посадку при различных метеоусловиях, узнаете о перегрузках, кроме того вы пройдете курс по фигурам высшего пилотажа и уничтожения воздушных целей."
			/>
			<Card
				imageSrc="url(./spitfire.jpg)"
				title="Воздушные сражения Spitfire Supermarine"
				text="Ощутите себя летчиком-асом. Поучаствуйте в самых известных сражениях Второй Мировой. Отразите атаки вражеских бомбардировщиков и истребителей, а также попробуйте сразиться с друзьями в дружеской воздушной дуэли."
			/>
		</div>

		{/* info tags */}
		<div className="container d-flex flex-row justify-content-between pt-2">
			<Label
				imageSrc="./avatar.svg"
				label="Авиатренажеры доступны для всех возрастов"
				style={{ padding: '40px 0 0 0' }}
			/>
			<Label
				imageSrc="./sunglasses.svg"
				label="Различные варианты полета для опытных пилотов"
				style={{ padding: '40px 0 0 0' }}
			/>
			<Label
				imageSrc="./around.svg"
				label="Доступны все аэропорты и места планеты"
				style={{ padding: '40px 0 0 0' }}
			/>
		</div>

		<div className="container d-flex flex-row justify-content-between">
			<Label
				imageSrc="./monitor.svg"
				label="Специальная подготовка и навыки не требуются"
				style={{ padding: '40px 0 0 0' }}
			/>
			<Label
				imageSrc="./propeller.svg"
				label="Дуэли и воздушные бои для большой компании"
				style={{ padding: '40px 0 0 0' }}
			/>
			<Label
				imageSrc="./pilot_hat.svg"
				label="Индивидуальный подход к каждому гостю"
				style={{ padding: '40px 0 0 0' }}
			/>
		</div>

		{/* Simulator description */}
		<div style={{ ...background, backgroundImage: `url(./simulator1.jpg)`, marginTop: '40px' }}>
			<div className="container">
				<h1 style={{ ...headerTitle, paddingTop: '100px' }}>Авиасимулятор? Что это?</h1>

				<div style={subtitle}>
					Авиатренажер достаточно точно<br />воспроизводит условия реального полета и<br />
					позволяет ощутить себя в роли<br />пилота гражданской авиации или летчика-<br />
					истребителя.<br />
				</div>

				<div style={{ ...subtitle, paddingTop: '10px' }}>
					Внешне и внутренне авиасимуляторы<br />представляют собой настоящие кабины<br />реальных самолетов,
					которые оснащены<br />оригинальными органами управления,<br />абсолютно рабочими приборами, и
					радиосвязью.<br />
				</div>

				<button className="btn btn-primary" style={{ ...blueBtn, marginBottom: '100px' }}>
					Узнать больше
				</button>
			</div>
		</div>

		{/* Contact section */}
		<div className="container">
			<div className="row" style={{ margin: '60px 0 60px 0' }}>
				<div className="col-4">
					<div style={blueHeader}>
						Координаты<br />и контакты
					</div>
					<Label
						imageSrc="./paper-plane.svg"
						height="40"
						label="av.simul@gmail.com"
						style={{ padding: '20px 0 0 0' }}
					/>
					<Label
						imageSrc="./phone-call.svg"
						height="40"
						label="077 52 51 612\n081 57 85 369"
						style={{ padding: '20px 0 0 0' }}
					/>
					<Label
						imageSrc="./placeholder.svg"
						height="40"
						label="Киев, ст м.Выставочный центр, Страна Роботов ВДНХ"
						style={{ padding: '20px 0 0 0' }}
					/>
					<Label
						imageSrc="./flight-time.svg"
						label="Вт-Вс с 10:00 до 20:00"
						height="40"
						style={{ padding: '20px 0 0 0' }}
					/>
				</div>
				<div className="col-8 pt-5 pb-5">
					<Map />
				</div>
			</div>
		</div>

		{/* Feedback */}
		<div style={{ ...background, backgroundImage: `url(./main_footer.jpg)`, marginTop: '70px', color: '#f6f6e9' }}>
			<div className="container p-5">
				<div className="row" style={{ paddingBottom: '160px' }}>
					<div className="col-6">
						<h3
							style={{
								fontSize: '35px',
								fontWeight: '400'
							}}
						>
							У вас есть вопросы?
						</h3>
						<div style={subtitle}>
							Оставьте ваш номер или позвоните нам и<br />
							мы ответим на все ваши вопросы,<br />
							расскажем о преимуществах каждой из<br />
							кабин и выберем тип полета для вас<br />
							после вы сможете сразу записаться на<br />
							подходящее для вас время и отправится в<br />
							полет!<br />
						</div>
						Ваш номер<br />
						<input type="tel" style={phoneInput} pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required />
						<br />
						<button className="btn btn-primary" style={darkerBtn}>
							Перезвоните мне
						</button>
					</div>
				</div>
				<Footer />
			</div>
		</div>
	</Page>
);

export default Home;
