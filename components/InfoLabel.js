import React from 'react';

export default ({ imageSrc, label, style, height, width }) => {
	let lines = label.split('\\n');
	let paddingTop = lines[0].length < 35 ? '10px' : '0px';

	height = !!height ? height : '50';
	width = height;

	// todo: fix image vertical align, when it's height < text block  height
	return (
		<div style={{ ...style, width: '350px' }}>
			<div style={{ color: '#005792' }}>
				<img
					src={imageSrc}
					style={{ float: 'left', marginRight: '20px', verticalAlign: 'middle' }}
					width={width}
					height={height}
				/>
				{lines.length > 1 ? (
					label.split('\\n').map((line) => (
						<span key={line}>
							{line}
							<br />
						</span>
					))
				) : (
					<p style={{ paddingTop }}>{lines[0]}</p>
				)}
			</div>
			<br style={{ clear: 'both' }} />
		</div>
	);
};
