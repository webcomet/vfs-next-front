import React from 'react';
import Link from 'next/link';

const links = [
	{ href: 'https://zeit.co/now', label: 'О Нас' },
	{ href: 'https://github.com/zeit/next.js', label: 'Авиасимулятор' },
	{ href: 'https://github.com/zeit/next.js', label: 'Магазин' }
].map((link) => {
	link.key = `nav-link-${link.href}-${link.label}`;
	return link;
});

const phoneNumber = {
	color: '#f6f6e9',
	fontSize: '17px',
	fontWeight: '500',
	paddingTop: '20px'
};

const Nav = () => (
	<div className="d-flex pt-2">
		<img src="./logo.png" />
		<nav>
			<ul>
				{links.map(({ key, href, label }) => (
					<li key={key}>
						<a href={href}>{label}</a>
					</li>
				))}
			</ul>
		</nav>
		<style jsx>{`
			nav {
				margin: 0 0 0 60px;
				text-align: center;
			}
			ul {
				display: flex;
				justify-content: space-between;
			}
			nav > ul {
				margin: 4px 0px;
			}
			li {
				display: flex;
				padding: 20px 20px;
			}
			a {
				position: relative;
				display: inline-block;
				vertical-align: top;

				color: #f6f6e9;
				text-decoration: none;
				font-size: 17px;
				font-weight: 500;
			}

			a:after {
				content: "";
				display: block;
				width: 100%;
				height: 3px;

				opacity: 0;
				background-color: #f6f6e9;

				position: absolute;
				top: 100%;
				left: 0;

				transition: opacity .1s linear;
			}

			a:hover:after {
				opacity: 1;
			}
		`}</style>
		<div className="ml-auto align-right">
			<span>
				<p style={phoneNumber}>
					<img src="./telephone.png" width="20" height="20" style={{ marginRight: '18px' }} />309 34 72 714
				</p>
			</span>
		</div>
	</div>
);

export default Nav;
