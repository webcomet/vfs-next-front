import React from 'react';
import GoogleMap from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const Map = (props) => {
	const defaultProps = {
		center: {
			lat: 59.95,
			lng: 30.33
		},
		zoom: 11
	};

	return (
		<div style={{ height: '520px', width: '100%' }}>
			<GoogleMap
				bootstrapURLKeys={{}}
				defaultCenter={!!props && !!props.center ? props.center : defaultProps.center}
				defaultZoom={!!props && !!props.zoom ? props.zoom : defaultProps.zoom}
			>
				<AnyReactComponent lat={defaultProps.center.lat} lng={defaultProps.center.lng} text="marker" />
			</GoogleMap>
		</div>
	);
};

export default Map;
