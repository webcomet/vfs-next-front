import React from 'react';

const cardStyle = {
	backgroundColor: '#13334c',
	color: '#f6f6e9',
	width: '350px',
	borderRadius: '0 0 30px 30px'
};

const cardImage = {
	width: '350px',
	height: '350px',
	backgroundRepeat: 'no-repeat',
	backgroundPosition: 'center',
	backgroundSize: 'cover'
};

const cardTitle = {
	fontSize: '18px',
	padding: '20px 30px 10px 30px',
	textAlign: 'left',
	lineHeight: '1.5'
};

const cardText = {
	padding: '0 30px 70px 30px'
};

export default ({ imageSrc, title, text }) => (
	<div className="flex" style={cardStyle}>
		<div style={{ ...cardImage, backgroundImage: imageSrc }} />
		<h3 style={cardTitle}>{title}</h3>

		<div style={cardText}>
			{text}
		</div>
	</div>
);
