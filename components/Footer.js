import React from 'react';

export default () => (
	<footer style={{ backgroundColor: 'transparent', color: '#f6f6e9' }}>
		<div className="row">
			<div className="col-2">
				<p>О нас</p>
				<p>Авиасимулятор</p>
			</div>
			<div className="col-2">
				<p>Контакты</p>
				<p>Магазин</p>
			</div>
			<div className="col-4 text-center">
				<img src="./logo.png" />
			</div>
			<div className="col-3 text-right my-auto">
        <img src="./instagram-logo-white.png" width="35" height="35"/>
      </div>
      <div className="col-1 my-auto">
        <img src="./facebook-letter-logo-white.png" width="35" height="35"/>
      </div>
		</div>
	</footer>
);
