import Head from 'next/head';

export default ({ children }) => (
	<div style={{ backgroundColor: '#f6f6e9' }}>
		<Head>
			<title>Home</title>
			<link rel="icon" href="/favicon.ico" />
			<link
				rel="stylesheet"
				href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
				integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
				crossorigin="anonymous"
			/>
			<link rel="stylesheet" href="fonts/avenir/stylesheet.css" type="text/css" />
		</Head>
		{children}
		<style>{`
			:global(body) {
			margin: 0;
			font-family: Avenir Next, Avenir, Helvetica, sans-serif;
			}`}</style>
	</div>
);
